<?php

class Model 
{
	public $DB;
	public $MC;
	public $Params;
	public $Result = ['Status' => false, 'Message' => '', 'Data' => ''];
	public $MenuArray = [];

	function __construct()
	{
		$this->DB = new DataBase();
		$this->Params['menu'] = $this->GetMenu();
		$this->Params['contact'] = $this->GetContactData();
		$this->Params['partners'] = $this->GetPartners();
		$this->Params['currency'] = $this->GetCurrency();
		$this->Params['meta'] = ['title' => Lang::Get('meta_title'), 'key' => Lang::Get('meta_key'), 'desc' => Lang::Get('meta_desc')];
		$this->Lang = Lang::GetLang();
		$this->LangId = Lang::GetLangID();
	}

	public function SetResult($Status, $Message = '', $Data = '')
	{
		$this->Result = ['Status' => $Status, 'Message' => $Message, 'Data' => $Data];
	}

	public function SetMetaData($Array)
	{
		if(isset($Array['view']) && isset($Array['metatitle']))
			$this->Params['meta'] = ['title' => $Array['metatitle'], 'key' => $Array['metakey'], 'desc' =>$Array['metadesc']];
		if(!isset($Array['view']) && isset($Array['metatitle']) && isset($Array['metakey']) && isset($Array['metadesc']))
			$this->Params['meta'] = $Array;
	}

	public function GetMenu()
	{	

		$Menu = $this->DB->GetAll('SELECT
                                  m.*,
                                  t.title
                                  FROM menu m
                                  LEFT JOIN menu_trans t ON t.menu_id = m.id
                                  WHERE t.language = ?i AND type = 1 AND published = 1
                                  ORDER BY ordering ASC', Lang::GetLangID());

		$Array =  array(
			'items' => array(),
			'parents' => array()
		);
		foreach ($Menu as $Item) {
			$Array['items'][$Item['id']] = $Item;
			$Array['parents'][$Item['parent']][] = $Item['id'];
		}
		return $Array;
	}

	public function GetContactData()
	{
		return $this->DB->GetRow('SELECT * FROM contact_info WHERE contact_lang_id = ?i', lang::GetLangID());
	}

	public function GetPartners()
	{
		return $this->DB->GetAll('SELECT * FROM partners ORDER BY ordering');
	}

	public function GetCurrency($type = '')
	{
		if($type){
			$currency = $type;
		}else{
			$currency = array('GEL','USD','EUR','RUB','GBP');
		}
		return $this->DB->GetAll('SELECT * FROM currency WHERE title IN (?a)', $currency);
	}

	public function Error(){
		header('location: '.URL.$this->Lang .'/error/');
		exit;
	}

}

?>