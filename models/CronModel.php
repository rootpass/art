<?php

class CronModel extends Model
{
	public function __construct()
	{		
		parent::__construct();
	}

	public function GetCurrency(){
		$Array = array('USD','EUR','RUB','GBP');
		$Data = Functions::GetCurrency($Array);
		if(!empty($Data)){	
			foreach ($Data as $Cur => $Val) {
				if(is_array($Val)){ 
					if($Cur == 'RUB'){
						$this->DB->Query('UPDATE currency SET currency = ?s, description = ?s WHERE title = ?s', $Val['Currency']/100, $Val['Description'], $Cur);
					}else{
						$this->DB->Query('UPDATE currency SET currency = ?s, description = ?s WHERE title = ?s', $Val['Currency'], $Val['Description'], $Cur);
					}
				}
			}                                             
		}
	}
}