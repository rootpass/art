<?php

class ProfileModel extends Model
{
    public $Count = 0;

    function __construct()
    {
        parent::__construct();
    }
    
    public function GetProfileData($Page, $PerPage)
    {
        $Start = $PerPage * ($Page - 1);
        $Limit = ' LIMIT ' . $Start . ', ' . $PerPage . '';
        $Data = $this->DB->GetAll('SELECT SQL_CALC_FOUND_ROWS t.*
                                  FROM profile_categories t
                                  ORDER BY t.title DESC '.$Limit.' '
                                  );
        $this->Count = $this->DB->GetOne('SELECT FOUND_ROWS()');
        return $Data;
    }

    public function GetPost($Page, $PerPage, $Id)
    {
        $Start = $PerPage * ($Page - 1);
        $Limit = ' LIMIT ' . $Start . ', ' . $PerPage . '';
        $Data = $this->DB->GetAll('SELECT SQL_CALC_FOUND_ROWS ft.title as file_title, f.file_name as image
                                  FROM profile_files f
                                  LEFT JOIN profile_files_trans ft ON ft.file_id = f.file_id
                                  WHERE f.category_id = ?i AND ft.lang_id = ?i
                                  ORDER BY f.ordering DESC '.$Limit.' ',
                                  $Id, Lang::GetLangID());
        $this->Count = $this->DB->GetOne('SELECT FOUND_ROWS()');
        return $Data;
    }

}