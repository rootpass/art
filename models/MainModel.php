<?php


class MainModel extends Model
{
	

	function __construct()
	{
		parent::__construct();
		$this->Params['slider'] = $this->GetSlider();
	}

	public function GetSlider()
	{	

		return	$this->DB->GetAll('SELECT
                                  s.*,
                                  t.slider_html, t.slider_caption, t.slider_link, t.slider_link_blank
                                  FROM slider s
                                  LEFT JOIN slider_trans t ON t.slider_id = s.slider_id
                                  WHERE t.slider_lang = ?i 
                                  ORDER BY slider_order ASC ', Lang::GetLangID());

	}


  public function Send()
  {
      if(Request::Post('yes_no') == 'on'){
        $On = 'თანხმობა';
      }else{
        $On = 'უარყოფა';
      }
      $Subject = SITE . ' - Contact';
      $Html = '
                      <b>პროდუქტის ტიპი - '.Request::Post('select_product').'</b><br><br>' .
                      '<h2>სესხის პირობები და მიზანი</h2><br><br>' .
                      'თანხა: ' . Request::Post('amount') . '<br>' .
                      'ვადა: ' . Request::Post('month_pop') . '<br>' .
                      'სესხის მიზანი: ' . Request::Post('load_reason') . '<br><br>' .
                      '<h2>სესხის პირობები და მიზანი</h2><br><br>' .
                      'სახელი გვარი: ' . Request::Post('name') . '<br>' .
                      'პირადი ნომერი: ' . Request::Post('unic_number') . '<br>' .
                      'დაბადების თარიღი: ' . Request::Post('birth_date') . '<br>' .
                      'მისამართი: ' . Request::Post('legal_address') . '<br>' .
                      'საკონტაკტო ნომერი: ' . Request::Post('phone_number'). '<br>' .
                      'სამუშაო ადგილი: ' . Request::Post('work_place') . '<br>' .
                      'პოზიცია: ' . Request::Post('work_position') . '<br>' .
                      'ხელფასის ოდენობა: ' . Request::Post('work_amount') . '<br>' .
                      '<h2>თანახმა ხართ თუ არა, რომ თქვენი მონაცემები გადამოწმდეს კრედიტინფოსა და სხვა მონაცემთა ბაზებში?</h2><br><br>' .
                      'პასუხი : ' . $On . '<br>';
      $Responce = Email::Send(EMAIL, Request::Post('email'), $Subject, $Html);
      if ($Responce) {
          $this->SetResult(1);
      } else {
          $this->SetResult(0);
      }
  }

}