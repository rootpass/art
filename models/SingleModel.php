<?php

/**
* single Model
*/
class SingleModel extends Model
{
	
	function __construct()
	{
		parent::__construct();

	}

	public function GetPage($PageId)
	{
		$Array = [];
		$Data = $this->DB->GetAll('
								SELECT p.*, t.* 
								FROM pages p 
								LEFT JOIN pages_trans t ON t.content = p.id 
								WHERE p.id = ?i',
							$PageId);
		foreach($Data as $val){
			$Array[$val['lang_id']] = $val;
		}
		$this->SetResult(true, '', $Array);
		return $this->Result;
	}

	public function Login($UserName, $Password)
	{
		if($UserName == '' || $Password == '')
			return 'შეავსეთ ყველა ველი';
		$UserData = $this->DB->GetRow('SELECT * FROM users WHERE role IN (1,2,3) AND username = ?s AND password = ?s AND status = 1', $UserName, md5($Password));
		if(!$UserData)
			return 'უზერი ან პაროლი არასწორია';
		else
			Session::Set('user', $UserData);
		return true;
	}

}