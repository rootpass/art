<?php

class ProfileModel extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function GetCategories()
    {
        $Data = $this->DB->GetAll('SELECT
                                    *
                                    FROM profile_categories ORDER BY title');
        $this->SetResult(true, '', $Data);
        return $this->Result;
    }

    public function GetCategoryData($Id)
    {
        $Data = $this->DB->GetRow('SELECT
                                    *
									FROM profile_categories
									WHERE category_id = ?i', (int)$Id);
        $this->SetResult(true, '', $Data);
        return $this->Result;
    }

    public function InsertCategory($Post)
    {
        $Oredring = $this->DB->GetOne('SELECT count(0)+1 FROM profile_categories');
        $this->DB->Query('INSERT INTO profile_categories SET ordering = ?i, title = ?i ', $Oredring, $Post['title']);

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function UpdateCategory($Id, $Post)
    {
        $this->DB->Query('UPDATE profile_categories SET ?u WHERE category_id = ?i', $Post, $Id);

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function RemoveCategory($Id)
    {
        $Count = $this->DB->GetOne('SELECT count(0) FROM profile_categories  WHERE category_id = ?i', $Id);
        if($Count){
            $this->SetResult(false, 'First remove all items');
            return $this->Result;
        }
        $this->DB->Query('DELETE FROM profile_categories WHERE category_id = ?i', $Id);
        $this->DB->Query('DELETE FROM profile_categories_trans WHERE category_id = ?i', $Id);

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function GetItems($Id)
    {
        $Data = $this->DB->GetAll('SELECT
									f.*,
									t.title,
									t.lang_id
									FROM profile_files f
									LEFT JOIN profile_files_trans t ON t.file_id = f.file_id
									WHERE f.category_id = ?i AND t.lang_id = 1
									ORDER BY ordering ASC', $Id);
        $this->SetResult(true, '', $Data);
        return $this->Result;
    }

    public function GetItemData($Id)
    {
        $Array = [];
        $Data = $this->DB->GetAll('SELECT
									f.*,
									t.title,
									t.lang_id
									FROM profile_files f
									LEFT JOIN profile_files_trans t ON t.file_id = f.file_id
									WHERE f.file_id = ?i AND t.lang_id = 1', (int)$Id);
        foreach($Data as $val){
            $Array[$val['lang_id']] = $val;
        }
        $this->SetResult(true, '', $Array);
        return $this->Result;
    }


    public function InsertItem($Category, $Post)
    {
        $Item['category_id'] = $Category;

        $this->DB->Query('INSERT INTO profile_files SET ?u', $Item);
        $Id = $this->DB->insertId();

        foreach ($Post as $Lang => $Val) {
            if(is_array($Val)){
                $Val['lang_id'] = $Lang;
                $Val['file_id'] = $Id;
                $this->DB->Query('INSERT INTO profile_files_trans SET ?u', $Val);
            }
        }

        if(Request::File('file')['name']){
            $Path = Request::File('file')['name'];
            $Ext = pathinfo($Path, PATHINFO_EXTENSION);

            $Microtime = ceil(microtime(false) * 1000).time();
            $Path = UPLOAD_PATH . 'files/' .$Microtime . '.'. $Ext;
            if (move_uploaded_file(Request::File('file')['tmp_name'], $Path))
            {
                $this->DB->Query('UPDATE profile_files SET file_name = ?s WHERE file_id = ?i', $Microtime.'.'.$Ext, $Id);
            }
        }

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function UpdateItem($Id, $Post)
    {
        foreach ($Post as $Lang => $Val) {
            if(is_array($Val)){
                $this->DB->Query('UPDATE profile_files_trans SET ?u WHERE file_id = ?i AND lang_id = ?i', $Val, $Id, $Lang);
            }
        }

        if(Request::File('file')['name']){
            $Path = Request::File('file')['name'];
            $Ext = pathinfo($Path, PATHINFO_EXTENSION);

            $Microtime = ceil(microtime(false) * 1000).time();
            $Path = UPLOAD_PATH . 'files/' .$Microtime . '.'. $Ext;
            if (move_uploaded_file(Request::File('file')['tmp_name'], $Path))
            {
                $this->DB->Query('UPDATE profile_files SET file_name = ?s WHERE file_id = ?i', $Microtime.'.'.$Ext, $Id);
            }
        }

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function RemoveItem($Id)
    {

        $File = $this->DB->GetOne('SELECT * FROM profile_files WHERE file_id = ?i', $Id);

        $this->DB->Query('DELETE FROM profile_files WHERE file_id = ?i', $Id);
        $this->DB->Query('DELETE FROM profile_files_trans WHERE file_id = ?i', $Id);

        $Path = UPLOAD_PATH . 'files/';
        @unlink($Path . $File);

        $this->SetResult(true, 'Success');
        return $this->Result;
    }

    public function SaveSort($Post)
    {
        $Order = explode(',', $Post['data']);
        foreach ($Order as $k => $id)
        {
            $i = $k+1;
            $this->DB->Query('UPDATE profile_files SET ordering =?i WHERE file_id = ?i', $i, $id);
        }

        $this->SetResult(true, 'Success');
        return $this->Result;
    }
}