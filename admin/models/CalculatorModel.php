<?php

class CalculatorModel extends Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function GetCalculatorData()
	{
		$Data = $this->DB->GetRow('SELECT * FROM calculator WHERE id = 1');
		$this->SetResult(true, '', $Data);
		return $this->Result;
	}

	public function Update($Post)
	{
        $this->DB->Query('UPDATE calculator SET ?u WHERE id = 1', $Post);
		$this->SetResult(true, 'Success');
		return $this->Result;
	}
}