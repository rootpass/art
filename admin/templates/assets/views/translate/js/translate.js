$(document).ready(function() {

	//filter pages
	$('.search').keyup(function() {
		if ($(this).val().length > 2) {
			listLangs($(this).val());
		} else {
			listLangs();
		}
	});
	var timer;
	//save lang
	$(document).on('keyup', '.lang-textarea', function() {
		clearTimeout(timer);
		var e = $(this);
		timer = setTimeout(function() {
			console.log(e);
			updateLangs(e);
		}, 2000);
	});
});
function listLangs(word) {
	$.ajax({
		url: URL + LANG + '/translate/getlangs/',
		type: 'POST',
		dataType: 'json',
		data: {
			ajax : true,
			word : word
		},
		success: function(data) {
			if (data.Status) {
				$('#langs-tbody').html(data.Data);
			}
		}
	});
}
function updateLangs(e) {
	$.ajax({
		url: URL + LANG + '/translate/update/',
		type: 'POST',
		dataType: 'json',
		data: {
			id : e.data('id'),
			lang : e.data('lang'),
			text : e.val(),
		},
		success: function(data) {
			$.jGrowl("Saved!");
		}
	});
}

