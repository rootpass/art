{% extends "index.tpl" %}

{% block content %}
    <div class="page-content">
        <div class="container-fluid">
            <form action="" method="post">
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2>{{ menuItem.title }}</h2>

                                <div class="subtitle"></div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!--.tabs-section-->
                                <div class="card">
                                    <div class="card-block row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უზრუნველყოფის გარეშე პროცენტი</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_1_percent" placeholder="Percent" value="{{ calculator.credit_1_percent }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უზრუნველყოფის გარეშე მინ თანხა</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_1_min_amount" placeholder="" value="{{ calculator.credit_1_min_amount }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უზრუნველყოფის გარეშე მაქს თანხა</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_1_max_amount" placeholder="" value="{{ calculator.credit_1_max_amount }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უზრუნველყოფის გარეშე მინ პერიოდი (თვე)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_1_min_period" placeholder="" value="{{ calculator.credit_1_min_period }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უზრუნველყოფის გარეშე მაქს პერიოდი (თვე)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_1_max_period" placeholder="" value="{{ calculator.credit_1_max_period }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="card-block row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უძრავი ქონებით უზრუნველყოფილი პროცენტი</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_2_percent" placeholder="Percent" value="{{ calculator.credit_2_percent }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უძრავი ქონებით უზრუნველყოფილი მინ თანხა</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_2_min_amount" placeholder="" value="{{ calculator.credit_2_min_amount }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უძრავი ქონებით უზრუნველყოფილი მაქს თანხა</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_2_max_amount" placeholder="" value="{{ calculator.credit_2_max_amount }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უძრავი ქონებით უზრუნველყოფილი მინ პერიოდი (თვე)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_2_min_period" placeholder="" value="{{ calculator.credit_2_min_period }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">სესხი უძრავი ქონებით უზრუნველყოფილი მაქს პერიოდი (თვე)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="credit_2_max_period" placeholder="" value="{{ calculator.credit_2_max_period }}" ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-block row">
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">დეპოზიტი ვადიანი პროცენტი (GEL)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="deposit_1_gel" placeholder="Percent" value="{{ calculator.deposit_1_gel }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">დეპოზიტი ვადიანი პროცენტი (USD)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="deposit_1_usd" placeholder="Percent" value="{{ calculator.deposit_1_usd }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">დეპოზიტი მოთხოვნამდე პროცენტი (GEL)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="deposit_2_gel" placeholder="Percent" value="{{ calculator.deposit_2_gel }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="form-control-label">დეპოზიტი მოთხოვნამდე პროცენტი (USD)</label>

                                                    <div class="">
                                                        <input type="text" class="form-control" name="deposit_2_usd" placeholder="Percent" value="{{ calculator.deposit_2_usd }}"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            {% if message %}
                                                <div class="alert alert-success alert-no-border alert-close alert-dismissible fade in" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    {{ message }}
                                                </div>
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <button type="submit" class="btn btn-inline">Change</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
{% endblock content %}

{% block footer %}
{% endblock footer %}
