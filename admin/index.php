<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'loader.php';

$MyApp = new App();
$MyApp->Init();
