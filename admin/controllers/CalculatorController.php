<?php

class Calculator extends Controller
{
	function __construct($Model, $ModelPath, $Menu)
	{
		parent::__construct($Model, $ModelPath);
		$this->Model->Params['menuItem'] = $Menu;
	}

	public function Index()
	{
		if(Request::Post()){
			$Result = $this->Model->Update(Request::Post());
			if($Result['Status'])
				$this->Model->Params['message'] = $Result['Message'];

		}
		$this->Model->Params['calculator'] = $this->Model->GetCalculatorData()['Data'];
		$this->View->render('calculator/index.tpl', $this->Model->Params);
	}
}