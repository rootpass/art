<?php

class Profile extends Controller
{
	function __construct($Model, $ModelPath, $Menu)
	{
		parent::__construct($Model, $ModelPath);
		$this->Model->Params['menuItem'] = $Menu;
	}

	public function Index()
    {
        $this->Model->Params['categories'] = $this->Model->GetCategories()['Data'];
        $this->View->render('profile/categories.tpl', $this->Model->Params);
    }

	public function AddCategory()
	{
		if(Request::Post()){
			$Result = $this->Model->InsertCategory(Request::Post());
			if($Result['Status'])
				header('location: '.URL.$this->Model->Lang .'/profile/');
			else
				$this->Model->Params['message'] = $Result['Message'];

		}
		$this->View->render('profile/inc/add_category.tpl', $this->Model->Params);
	}

	public function EditCategory($Id)
	{
		if(Request::Post()){
			$Result = $this->Model->UpdateCategory($Id, Request::Post());
			if($Result['Status'])
				header('location: '.URL.$this->Model->Lang .'/profile/');
			else
				$this->Model->Params['message'] = $Result['Message'];

		}
		$this->Model->Params['category'] = $this->Model->GetCategoryData($Id)['Data'];
		//Functions::Pre($this->Model->Params['category']);
		if(!$this->Model->Params['category'])
			$this->Model->Error();

		$this->View->render('profile/inc/edit_category.tpl', $this->Model->Params);
	}

	public function RemoveCategory($Id)
	{
		$Result = $this->Model->RemoveCategory($Id);
		echo json_encode($Result);
	}

	public function Items($Id)
	{
		$this->Model->Params['items'] = $this->Model->GetItems($Id)['Data'];
		$this->View->render('profile/items.tpl', $this->Model->Params);
	}

	public function AddItem($Category)
	{
		if(Request::Post()){
			$Result = $this->Model->InsertItem($Category, Request::Post());
			if($Result['Status'])
				header('location: '.URL.$this->Model->Lang .'/profile/items/'.$Category.'/');
			else
				$this->Model->Params['message'] = $Result['Message'];

		}
		$this->View->render('profile/inc/add_item.tpl', $this->Model->Params);
	}

	public function EditItem($Category, $Id)
	{
		if(Request::Post()){
			$Result = $this->Model->UpdateItem($Id, Request::Post());
			if($Result['Status'])
				header('location: '.URL.$this->Model->Lang .'/profile/items/'.$Category.'/');
			else
				$this->Model->Params['message'] = $Result['Message'];

		}
		$this->Model->Params['item'] = $this->Model->GetItemData($Id)['Data'];
		//Functions::Pre($this->Model->Params['images']);
		if(!$this->Model->Params['item'])
			$this->Model->Error();

		$this->View->render('profile/inc/edit_item.tpl', $this->Model->Params);
	}

	public function RemoveItem($Id)
	{
		$Result = $this->Model->RemoveItem($Id);
		echo json_encode($Result);
	}

	public function SaveSort()
	{
		$Result = $this->Model->SaveSort(Request::Post());
		echo json_encode($Result);
	}
}