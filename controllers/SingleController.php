<?php

class Single extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);

		if(Request::Req('Ajax'))
			return;
	}

	public function Index()
	{
		if(!$this->Menu['content'])
			$this->Error();

		$this->Model->Params['Cover'] = $this->Menu['image'];
		$this->Model->Params['data'] = $this->Model->GetPage($this->Menu['content'])['Data'][Lang::GetLangID()];
        //Functions::Pre($this->Model->Params['data']);
		$this->View->render('single/index.tpl', $this->Model->Params);
	}
	

	public function Login()
	{
		if(Request::Post('username') &&  Request::Post('password')){
			$UserData = $this->Model->Login(Request::Post('username'), Request::Post('password'));
			if($UserData){
				$this->Model->Params['message'] = $UserData;
				header('location: '.URL.Lang::GetLang().'/profile');
			}
			else
				header('location: '.URL.Lang::GetLang().'/investors');
		}
		$this->View->render('single/index.tpl', $this->Model->Params);
	}

	public function Logout()
	{
		Session::Destroy('user');
		header('location: '.URL.Lang::GetLang().'/investors/');
	}

}