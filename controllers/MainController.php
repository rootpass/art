<?php

/**
* Main Controller
*/
class Main extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
	}

	public function Index()
	{
		$this->Model->Params['Slides'] = $this->Model->GetSlider();
		//Functions::Pre($this->Model->Params['News']);
		$this->View->render('main/index.tpl', $this->Model->Params);
	}

	public function Send()
	{
		$this->Model->Send();
		if( $this->Model->Result['Status'] ){
			header('location: '.URL.Lang::GetLang().'/');
		}
		
	}

}