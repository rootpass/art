<?php

class Profile extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
		if(!Session::Get('user')){
			header('location: '.URL.Lang::GetLang().'/investors');
		}
	}

	public function Index()
	{
        $this->Model->Params['Page'] = Request::Get('page') ? (int)Request::Get('page') : 1;
        $this->Model->Params['PerPage'] = 40;

		$this->Model->Params['Profile'] = $this->Model->GetProfileData($this->Model->Params['Page'], $this->Model->Params['PerPage']);

		$this->Model->Params['Count'] = $this->Model->Count;
		$this->Model->Params['Cover'] = $this->Menu['image'];
		$this->View->render('profile/index.tpl', $this->Model->Params);
	}

	public function Detail($Id = false)
	{
		if(empty($Id))
			$this->Model->Error();

		$this->Model->Params['Page'] = Request::Get('page') ? (int)Request::Get('page') : 1;
        $this->Model->Params['PerPage'] = 40;

		$this->Model->Params['Post'] = $this->Model->GetPost($this->Model->Params['Page'], $this->Model->Params['PerPage'],$Id);
		$this->Model->Params['Cover'] = $this->Menu['image'];
		$this->View->render('profile/detail.tpl', $this->Model->Params);
	}

}