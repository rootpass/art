<?php

class Cron extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		set_time_limit(0);
	}
	
	public function Index()
	{
		echo 'Index';
	}

	public function Minutes1()
	{
		//
	}

	public function Minutes5()
	{
		//
	}

	public function Hour1()
	{
		//
	}

	public function Day1()
	{
		//
	}

	public function LangsGenerator()
	{
		$Langs = new LangsGenerator();
		$Langs->FillLangs();
		$Langs->SaveLangFiles();
		// for admin
		$Langs = new LangsGenerator(true);
		$Langs->FillLangs();
		$Langs->SaveLangFiles();
	}
	
	public function TmpClear()
	{
		foreach(scandir(dirname(__FILE__) . '/../tmp/') as $Tmp)
		{
			if($Tmp != '.' && $Tmp != '..')
			{
				if((time() - filemtime(dirname(__FILE__) . '/../tmp/' . $Tmp)) > 1800)
				{
					// delete
					unlink(dirname(__FILE__) . '/../tmp/' . $Tmp);
				} 
			}
		}
	}

	public function GetCurrency(){
		$this->Model->GetCurrency();
	}
}