<?php


class Contact extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
	}

	public function Index()
	{
		$this->Model->Params['Cover'] = $this->Menu['image'];
		$this->View->render('contact/index.tpl', $this->Model->Params);
	}

	public function Send()
	{
		$this->Model->Send();
		if( $this->Model->Result['Status'] ){
			header('location: '.URL.Lang::GetLang().'/contact');
		}
		
	}

	

}