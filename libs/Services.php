<?php

/**
* Services Client
*/
class Services
{
	private static $URL    = SERVICES_URL;
	private static $Hash   = SERVICES_HASH;
	private static $SiteID = SITE_ID;

	public static function Post($Func, $ShowType = 'JSON', $Params = null)
	{
		$Params 	= is_array($Params) ? $Params : [];
		$PostData 	= array_merge(
			array(
				'Func'     => $Func,
				'LangID'   => Lang::GetLangID(),
				'ShowType' => $ShowType
			),
			Request::Req(),
			$Params,
			array(
				'Hash'     	=> self::$Hash,
				'SiteID'   	=> self::$SiteID,
				'UserID' 	=> Session::Get('UserID'),
				'IP'   		=> $_SERVER['REMOTE_ADDR']
			)
		);

		$Data = Curl::Post(self::$URL, $PostData);

		return strtoupper($PostData['ShowType']) == 'ARRAY' ? json_decode($Data, true) : $Data;
	}
}