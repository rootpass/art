{% extends "index.tpl" %}

{% block content %}

{% include 'inc/cover.tpl' %}
<div class="container profile-min-height">
    <div class="row">
        {% for pr in Profile %}
        <div class="col-md-1 col-xs-2">
            <a class="folder-container" href="{{ globals.uri }}profile/detail/{{ pr.category_id }}">
                <img class="folder" src="{{constants.THEME}}assets/img/folder.png" alt="">
                <span>{{pr.title}}</span>
            </a>
        </div>
        {% endfor %}
    </div>
</div>
{% endblock %}