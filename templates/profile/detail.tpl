{% extends "index.tpl" %}

{% block head %}
{% endblock %}

{% block content %}

{% include 'inc/cover.tpl' %}
<div class="container">
	{% for det in Post %}
	{% set file = det.image|split('.')  %}
	{% if file[1] == 'xls' or file[1] == 'xlsx' %}
		{% set img = 'excell.png' %}
	{% elseif file[1] == 'docx' %}
		{% set img = 'word.png' %}
	{% elseif file[1] == 'pdf' %}
		{% set img = 'pdf.png' %}
	{% endif %}
    <a class="excell-row" href="{{ globals.url }}upload/files/{{det.image}}">
        <img src="{{constants.THEME}}assets/img/{{img}}" alt="">
        <span>{{det.file_title}}</span>
    </a>
    {% endfor %}
</div>
{% endblock %}