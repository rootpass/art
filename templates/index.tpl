{% spaceless %}
    <!DOCTYPE html>
    <html lang="{{ globals.lang.code }}">
    <head>
        <meta charset="UTF-8">
        <title>{{ meta.title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="language" content="{{ globals.lang.code }}"/>
        
        <meta name="description" content="{{ meta.desc }}"/>
        <meta name="keywords" content="{{ meta.key }}"/>
        <meta name="copyright" content="Copyright &copy; {{ date('Y') }}"/>
        <!-- Facebook Meta tags -->
        <!--<meta property="fb:app_id" content="361736430535656" /> -->
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="{{ meta.title }}"/>
        <meta property="og:description" content="{{ meta.desc }}"/>
        <meta property="og:url" content="{{ globals.uri }}"/>
        {% if globals.controller == 'news' and globals.action == 'detail' %}
            {% for i in Images %}
                <meta property="og:image" content="{{ constants.UPLOAD }}news/l_{{ i.content_photo_name }}"/>
            {% endfor %}
        {% endif %}

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" type="text/css"  href="css/bootstrap.min.css">
        <!-- Template Stylesheets -->
        <link rel="stylesheet" type="text/css"  href="css/style.css">
        <!-- Custom Animations -->
        <link rel="stylesheet" type="text/css"  href="css/animate.min.css">
        <!-- Font Icons -->
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/etlinefont.css">
        <!-- Owl Carousel Css -->
        <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
        <link href="css/owl.theme.css" rel="stylesheet" media="screen">
        <!-- Magnific Popup Css -->
        <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
        <!-- Style Switcher -->
        <link href="css/switcher.css" rel="stylesheet">
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="{{ constants.THEME }}assets/css/src/style.css"/>
 <!--        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->

        <link rel="shortcut icon" type="image/png" href="{{ constants.THEME }}assets/img/favicon.png"/>
        {% block head %}

        {% endblock %}
        <script src="{{ constants.THEME }}assets/js/build/libs/jquery.min.js{{ constants.ver }}"></script>
        <script type="text/javascript">
            var LANG_ID = {{ globals.lang.id }};
            var LANG = '{{ globals.lang.code }}';
            var THEME = '{{ constants.THEME }}'; 
            var URL = '{{ globals.url }}';
            var URI = '{{ globals.uri }}';
        </script>
    </head>

    <body>
    <!-- PRELOADER -->
      <div class="preloader">
         <div class="loader"></div>
      </div>
      <!-- Navigation -->
      <nav id="main-menu" class="navbar navbar-default navbar-fixed-top">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="index.html"> MULTI</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="#home" class="page-scroll">Home</a></li>
                  <li><a href="#about" class="page-scroll">About Us</a></li>
                  <li><a href="#works" class="page-scroll">Works</a></li>
                  <li><a href="#Contact" class="page-scroll">Contact</a></li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>

    {% block content %}

    {% endblock %}

    <footer id="footer">
        <div class="container">
            <p>&copy; 2017 MULTI Studios. All Rights Reserved.</p>
            <ul class="footer-social">
               <li><a href="#"><i class="fa fa-behance"></i></a></li>
               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
               <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
               <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
        </div>
        {% block footer %}
        {% endblock %}
        <script>
            {% block js %}
                {{ load('lang') | raw }}
            {% endblock %}
        </script>
    </footer>
    <!-- jQuery Version 2.1.3 -->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Plugin JavaScript -->
      <script type="text/javascript" src="js/SmoothScroll.js"></script>
      <script type="text/javascript" src="js/jquery.isotope.js"></script>
      <script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
      <script type="text/javascript" src="js/owl.carousel.js"></script>
      <script type="text/javascript" src="js/wow.min.js"></script>
      <script type="text/javascript" src="js/ajax-mail.js"></script>
      <!-- Google Map JavaScript -->
      <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
      <script src="js/map.js"></script>
      <!-- Custom Theme JavaScript -->
      <script type="text/javascript" src="js/main.js"></script>
    </body>
    </html>
{% endspaceless %}