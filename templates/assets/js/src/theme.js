// Check Email
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
var contact = {
    send: function(obj) {
        if(jQuery.trim(jQuery('#name').val()) == ''){
            jQuery('#name').focus();
            jQuery('#name').css({"border-right": "2px solid red"});
            return false;
        }
        if(!isEmail(jQuery('#email').val())){
            jQuery('#email').focus();
            jQuery('#email').css({"border-right": "2px solid red"});
            return false;
        }
        if(jQuery.trim(jQuery('#message').val()) == ''){
            jQuery('#message').focus();
            jQuery('#message').css({"border-right": "2px solid red"});
            return false;
        }
        var loader = jQuery('.contact-loader');
        loader.show();
        jQuery.post(jQuery(obj).attr('action'), jQuery(obj).serializeArray(), function(data){
            data = jQuery.parseJSON(data);
            loader.hide();
            if(data.Status == 1){
                jQuery('.contact-message').html('<span class="success">' + lang.message_send_success + '</span>');
                jQuery('.contact-message').show();
            } else {
                jQuery('.contact-message').html('<span class="error">' + lang.message_send_error + '</span>');
                jQuery('.contact-message').show();
            }
            setTimeout(function () {
                jQuery('.contact-message').hide();
            }, 8000)
        });
        return false;
    }
}


jQuery(function($) {

    var config = $('html').data('config') || {};
    
    // Fix for RTL sliders
    UIkit.on('beforeready.uk.dom', function () {  
        UIkit.$('[data-uk-slideset],[data-uk-slider]').attr('dir', 'ltr');  
    });

    
   jQuery('.our-team-wrap .team-read-more').click(function(){
       jQuery(this).parent().parent().find('.additional').slideToggle();
       return false;
   });

   jQuery('.teamdonate-form label').click(function() {
        jQuery(this).addClass('active').siblings().removeClass('active');

    });
});

jQuery(window).load(function(){
    
    jQuery('.preloader').fadeOut();

    colWidth = jQuery('.grid').width();

    $grid = jQuery('.grid');
    $grid.isotope({
        // options
        itemSelector: '.grid-item',
        percentPosition: true,
        layoutMode: 'masonry',
        masonry: {
            columnWidth: $grid.find('.grid-item')[1]
        }

    });


    jQuery('.filter-button-group').on('click', 'button', function() {
        $grid.isotope({
            // options
            itemSelector: '.grid-item',
            percentPosition: true,
            layoutMode: 'masonry',
            masonry: {
                columnWidth: $grid.find('.grid-item')[1]
            }

        });
        var filterValue = jQuery(this).attr('data-filter');
        jQuery(this).toggleClass('active').siblings().removeClass('active');
        $grid.isotope({
            filter: filterValue
        });
    });

    colWidth1 = jQuery('.grid1').width();
    $grid1 = jQuery('.grid1');    
        $grid1.isotope({
            // options
            itemSelector: '.player-item',
            percentPosition: true,
            layoutMode: 'masonry',
            masonry: {
                columnWidth:  $grid1.find('.player-item')[1]
              }

        });  
   
    
    jQuery('.filter-button-group').on( 'click', 'button', function() {
        $grid1.isotope({
            // options
            itemSelector: '.player-item',
            percentPosition: true,
            layoutMode: 'masonry',
            masonry: {
                columnWidth:  $grid.find('.player-item')[1]
              }

        }); 
        var filterValue = jQuery(this).attr('data-filter');
        jQuery(this).toggleClass('active').siblings().removeClass('active');
        $grid1.isotope({ filter: filterValue });
    });
    
});