$(function () {
    calculation('from');
});
function calculation(type)
{
    var fromCourse = $('select[name=cur_from]').find('option:selected').data('currency');
    var toCourse = $('select[name=cur_to]').find('option:selected').data('currency');
    var fromValue = $('input[name=value_from]').val();
    var toValue = $('input[name=value_to]').val();

    if(type == 'from' && fromValue) {
        res = fromValue * fromCourse / toCourse;
        res = (Math.round(res * 10000) / 10000).toFixed(2);
        console.log(res);
        $('input[name=value_to]').val(res);
    }
    if(type == 'to' && toValue){
        res = toValue*toCourse/fromCourse;
        res = (Math.round(res*10000)/10000).toFixed(2);
        $('input[name=value_from]').val(res);
    }
}
