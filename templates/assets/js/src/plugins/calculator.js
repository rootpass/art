$(function () {
    $(document).on('change','#credit-type',function(){
    	var loanType = $('#credit-type').children('option:selected').data('type');
    	if(loanType == 2){
    		$('#loan-amount').attr('placeholder','მაქს 5000');
    		$('#loan-period').attr('placeholder','მაქს 30');
    	}else{
    		$('#loan-amount').attr('placeholder','მაქს 50000');
    		$('#loan-period').attr('placeholder','მაქს 60');
    	}
    });
});

function calculateDeposit(){
	var depositType = $('select[name=deposit]').val();
	var valute = $('select[name=valute]').val();
	var price = $('input[name=price]').val();
	var month = $('input[name=month]').val();
	var countPersent = ''; var monthlyCome = ''; var sumCome = '';

	if(depositType == 1 && valute == 1)
		countPersent = $('input[name=percentData]').data('usd');
	else if(depositType == 2 && valute == 1)
		countPersent = $('input[name=percentData]').data('usd1');
	else if(depositType == 1 && valute == 2)
		countPersent = $('input[name=percentData]').data('gel');
	else if(depositType == 2 && valute == 2)
		countPersent = $('input[name=percentData]').data('gel1');

	if(price && month && countPersent){
		if(month < 6){
			bootbox.alert("მინიმალური თვეების რაოდენობა 6");
			return false;
		}
		monthlyCome = Math.round( ((countPersent/12)*price)/100 );
		sumCome = Math.round( monthlyCome*month );
		$('#yearcome').val(countPersent+'%');
		$('#monthcome').val(monthlyCome);
		$('#sumcome').val(sumCome);
	}else{
		bootbox.alert('გთხოვთ შეავსოთ ყველა ველი');
		return false;
	}
}

function calculateLoan(){
	//p = x*(1 - (1+r)^-n)/r
	var interest = parseFloat($('#credit-type').val())/100/12;
	var principal = parseFloat($('#loan-amount').val());
	var payment = parseFloat($('#loan-period').val());
	var loanType = $('#credit-type').children('option:selected').data('type');
	if(!interest || !principal || !payment){
		bootbox.alert('გთხოვთ შეავსოთ ყველა ველი');
		return false;
	}
	if(loanType == 1 && principal > 50000){
		bootbox.alert('მაქსიმალური თანხა 50000');
		return false;
	}

	if(loanType == 2 && principal > 5000){
		bootbox.alert('მაქსიმალური თანხა 5000');
		return false;
	}
	if(loanType == 1 && payment > 60){
		bootbox.alert("მახსიმალური თვეების რაოდენობა 60");
		return false;
	}
	if(loanType == 2 && payment > 30){
		bootbox.alert("მახსიმალური თვეების რაოდენობა 30");
		return false;
	}
	// Now compute the monthly payment figure.
	var x = Math.pow(1 + interest, payment);   // Math.pow() computes powers
	var monthly = (principal*x*interest)/(x-1);

	$('#loan-percent').val(parseFloat($('#credit-type').val()) + '%');
	$('#loan-monhtly').val( Math.round( monthly ) );
	$('#loan-total').val( Math.round( ((monthly*payment)-principal) ) );
}