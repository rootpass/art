$(document).ready(function() {
    // modal controls
    $('#modal__open').on('click', function() {
        $('.modal-bg').show();
    });

    $(document).on('change', '#check', function(e) {
        if (this.checked) {
            $('#form__submit').prop('disabled', false);
        } else {
            $('#form__submit').prop('disabled', true);
        }
        e.preventDefault();
    });

    $(document).on('change','#credit-type-pop',function(){
        var loanType = $('#credit-type-pop').children('option:selected').data('type');
        if(loanType == 2){
            $('#loan-amount-pop').attr('placeholder','მაქს 5000');
            $('#loan-period-pop').attr('placeholder','მაქს 30');
        }else{
            $('#loan-amount-pop').attr('placeholder','მაქს 50000');
            $('#lloan-period-pop').attr('placeholder','მაქს 60');
        }
    });

    $('.modal-container select').change(function() {
        $(this).closest('.modal__content').removeClass('active');
        $(this).closest('.modal__content').next().addClass('active');
    });

    $('.modal-container .btn-next').on('click', function(e) {
        var Empty = $('input');
        var Name = $('input[name="name"]');
        var Id = $('input[name="unic_number"]');
        var Phone = $('input[name="phone_number"]');
        var loanType = $('#credit-type-pop').children('option:selected').data('type');
        var principal = parseFloat($('#loan-amount-pop').val());
        var payment = parseFloat($('#lloan-period-pop').val());

        if(loanType == 1 && principal > 50000){
            bootbox.alert('მაქსიმალური თანხა 50000');
            return false;
        }
        if(loanType == 2 && principal > 5000){
            bootbox.alert('მაქსიმალური თანხა 5000');
            return false;
        }
        if(loanType == 1 && payment > 60){
            bootbox.alert("მახსიმალური თვეების რაოდენობა 60");
            return false;
        }
        if(loanType == 2 && payment > 30){
            bootbox.alert("მახსიმალური თვეების რაოდენობა 30");
            return false;
        }
        if (Name.val() != '' && Id.val() != '' && Phone.val()) {
            $(this).closest('.modal__content').removeClass('active');
            $(this).closest('.modal__content').next().addClass('active');
        }

        function updateInputClass() {
            $(this).toggleClass('not-valid', $.trim(this.value) == '');
            return true;
        }
        $(".required").blur(updateInputClass).each(updateInputClass);


    });

    $('.modal-container .btn-prev').on('click', function() {
        $(this).closest('.modal__content').removeClass('active');
        $(this).closest('.modal__content').prev().addClass('active');
    });

    $('.fa-window-close').on('click', function() {
        $('.modal-bg').hide();
    });

    var MainSlider = new Swiper(".swiper-container", {
        speed: 400,
        spaceBetween: 0,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        autoplay: 5600
    });

    // calculator tabs
    $("#home a").click(function(e) {
        e.preventDefault()
        $(this).tab("show")
    });
    $("#profile a").click(function(e) {
        e.preventDefault()
        $(this).tab("show")
    });

    // products sub menu
    $(".menu-links").mouseenter(function() {
        $(this).children(".sub-menu").addClass("active");
        if ($(this).find(".sub-menu").length != 0) {
            $(".bg").addClass("active");
        }
    });
    // products sub menu
    $(".menu-links").mouseleave(function() {
        $(".bg, .sub-menu").removeClass("active");
    });

});




$(document).on('keydown', '.numeric', function(e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything 
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
    if ($(this).val().length == 7 && this.selectionStart == 7)
        e.preventDefault();
});