{% extends "index.tpl" %}

{% block content %}
    <div class="container">
        <div class="error">
            {{ langs.page_not_found }} <br>
            404
        </div>
    </div>
{% endblock %}