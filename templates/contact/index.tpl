{% extends "index.tpl" %}

{% block head %}
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/mootools/1.3.1/mootools-yui-compressed.js'></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAbFuepJJ9CxjpIwK8A8OW9kxvL9OWm9Ts"></script>
{% endblock %}

{% block content %}
    {% include 'inc/cover.tpl' %}
    <div class="container contact">
        <div class="row">
            <div class="col-md-7">
                <h2>{{ langs.contact_info }}</h2>
                <ul class="list-unstyled">
                    <li><img src="{{ constants.THEME }}assets/img/pin_green.png" alt="">{{ contact.contact_address }}</li>
                    <li><img src="{{ constants.THEME }}assets/img/phone_green.png" alt=""><a href="tel:{{ contact.contact_phone1 }}">{{ contact.contact_phone1 }}</a></li>
                    <li><img src="{{ constants.THEME }}assets/img/message_green.png" alt="">
                        <a href="mailto:{{ contact.contact_email1 }}">{{ contact.contact_email1 }}</a> 
                    </li>
                    <li class="contact-word-icon">
                        <img src="{{ constants.THEME }}assets/img/word.png" alt="">
                        <a target="_blank" href="{{globals.url}}upload/files/prezentacia.docx">პრეტენზიის სტანდარტული ფორმა</a></li>
                </ul>
            </div>
            <div class="col-md-5 contact-form">
                <div class="panel panel-default">
                    <div class="container-fluid ">
                        <form method="post" action="{{ globals.uri }}contact/send">
                            <h3>{{ langs.contact_us }}</h3>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="{{ langs.name }}">
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="{{ langs.email }}">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="{{ langs.message }}" rows="8"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="row container-fluid">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-default">{{ langs.send }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="forms-group map__ container-fluid">
                <img src="{{ constants.THEME }}assets/img/map.png" alt="">
            </div>
        </div>
    </div>
    <script>
        {% set loc = contact.contact_location | split(',') %}
        window.map = false;
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ loc[0] }} , {{ loc[1] }});
            var mapOptions = {
                zoom:{{ loc[2] }},
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                streetViewControl: false,
                overviewMapControl: false,
                mapTypeControl: false
            };

            window.map = new google.maps.Map(document.getElementById('map'), mapOptions);

            var contentString = '<div id="content" style="height:40px;">' +
                    '<div>' +
                    '</div>' +
                    '<h2 class="firstHeading" style="font-size:18px;margin:0;">{{ contact.contact_title }}/h2>' +
                    '<div id="bodyContent">' +
                    '</div>' +
                    '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Sports Club Tusheti'
            });

            //infowindow.open(map, marker);

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }


        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
{% endblock %}