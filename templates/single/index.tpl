{% extends "index.tpl" %}

{% block content %}
	{% include 'inc/cover.tpl' %}
    <div class="container">
    	<div class="container investor products">
	    	<div class="row">
				<div class="col-md-8">
					{{ data.text | raw }}
				</div>
	            {% if globals.controller == 'investors' %}
	            	<div class="col-md-4">
		            {% include 'inc/login.tpl' %}
		        	</div>
	            {% endif %}
	        </div>
		</div>
    </div>
{% endblock %}