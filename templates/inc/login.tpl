{% if session['user']['id'] %}
<div class="fomr-group user-logout">
    <img src="{{constants.THEME}}assets/img/user-logged.png" alt="" >
    <a href="{{ globals.uri }}profile"><span>{{session['user']['username']}}</span></a>
    <a href="{{ globals.uri }}single/logout" class="pull-right text-center btn-primary">გასვლა</a>
</div>
<!--<div class="container-fluid">
    Profile: {{session['user']['username']}}</a>
</div>-->
{% else %}
<div class="panel panel-default login">
    <div class="container-fluid">
        <h3><img src="{{constants.THEME}}assets/img/user.png" alt=""> {{langs.avtorization}}</h3>
        <form method="post" action="{{ globals.uri }}single/login" class="">
            <div class="form-group">
                <label for="1">{{langs.username}}</label>
                <input type="text" name="username" class="form-control" id="1">
            </div>
            <div class="form-group">
                <label for="2">{{langs.password}}</label>
                <input type="Password" name="password" class="form-control" id="2">
            </div>
            <div class="form-group">
                <div class="row container-fluid">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-default"><span>{{langs.avtorization}}</span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
{% endif %}