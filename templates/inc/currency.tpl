<div class="panel panel-default panel-currency">
    <div class="panel-heading">{{ langs.valute_course }}</div>
    <div class="panel-body">
        <div class="col-md-6 col-xs-6">
            <div class="row title"></div>
            {% for cur in currency %}
                {% if cur.title != 'GEL'%}
                	<div class="row"><span>{{cur.title}}</span> <img src="{{constants.THEME}}assets/img/{{cur.title|lower}}.png" alt=""></div>
                {% endif %}
           	{% endfor %}
        </div>
        <div class="col-md-6 courses col-xs-6">
            <div class="row title">
                <p>{{ langs.buy }}</p>
            </div>
            {% for cur in currency %}
                {% if cur.title != 'GEL'%}
                    <div class="row">
                        <p>{{cur.currency}}</p>
                    </div>
                {% endif %}

           	{% endfor %}
        </div>
    </div>
</div>