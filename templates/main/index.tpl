{% extends "index.tpl" %} {% block content %}

<div class="container-fluid">
    <div class="row">
        <!-- Slider main container -->
        <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                {% for s in Slides %}
                <div class="swiper-slide">
                    <img src="{{ constants.UPLOAD }}slider/{{ s.slider_image }}" alt="Smart Invest">
                    {% if s.slider_caption %}
                        <div class="event">
                            {{s.slider_caption}}
                        </div>
                    {% endif %}
                </div>
                {{ s.slider_html ? s.slider_html | raw }} {% if s.slider_link %} {#
                <div class="clear"></div>
                <a href="{{ s.slider_link }}" class="read-more" {{ s.slider_link_blank ? 'target="_blank"' }}>{{ langs.read_more }}</a>#} {% endif %} {% endfor %}

            </div>
            <!-- If we need pagination -->
            <!-- <div class="swiper-pagination"></div> -->

            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>

            <!-- If we need scrollbar -->
            <!-- <div class="swiper-scrollbar"></div> -->
        </div>
    </div>
</div>
{% endblock %}

{% block footer %}
   <script src="{{ constants.THEME }}assets/js/build/plugins/convertor.min.js{{ constants.ver }}"></script>
{% endblock %}
