<?php
// Define site address
if(dirname($_SERVER['PHP_SELF']) == DIRECTORY_SEPARATOR)
	$SysPath = '/';
else
	$SysPath = dirname($_SERVER['PHP_SELF']).'/';
if(isset($_SERVER['HTTPS']))
	$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
else
	$protocol = 'http';
define('URL', $protocol . "://" . $_SERVER['HTTP_HOST'].$SysPath);
define('DIR', $SysPath);

if($_SERVER['HTTP_HOST'] == 'localhost')
    define('BRANCH', 'MASTER');
else
    define('BRANCH', 'MASTER');

define('SITE', 'iraklitwuladze.com');
define('EMAIL', 'beqa28282@gmail.com');
define('SMTP', false);
define('ROOT', dirname(__FILE__) . '/');
define('LIBS', 'libs/');
define('THEME', URL . 'templates/');
define('UPLOAD', URL . 'upload/');
define('UPLOAD_PATH', ROOT . 'upload/');
define('TMP', URL . 'tmp/');
define('NO_PHOTO', THEME . 'images/no_photo.png');
define('CACHE', 'cache/');
define('LANGS', json_encode(['ka' => ['id' => 1, 'title' => 'GEO'], 'en' => ['id' => 2, 'title' => 'ENG']]));
define('LANGS_DIR', 'langs/');
define('DEFAULT_LANG', 'ka');
define('IMG_TYPES', serialize(['image/png', 'image/jpg', 'image/jpeg', 'image/gif']));
define('IMG_TYPE', '.jpg');
define('FB_APP_ID', -1);

define('FILE_CACHE_DIR', DIR . 'cache/');
define('FILE_CACHE_LIFE_TIME', 3600);

//Twig
define('TWIG_CACHE', true);
define('TWIG_CACHE_PATH', 'cache/twig/');
define('TWIG_TPL', ROOT . 'templates/');
define('ADMIN_TWIG_TPL', ROOT . 'admin/templates/');

//files
define('ver', '?v=0.125');

//Cookie
define('COOKIE_SITE_TIME', 30); //in days
define('COOKIE_SITE_DOMAIN', ''); //ex: localhost

//database
define('DB_HOST', 'localhost');
define('DB_NAME', 'art');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_DEBUG', false);

// Admin Configs
define('ADMIN_LIBS', '../libs/');
define('ADMIN_LANGS', json_encode(['ka' => ['id' => 1, 'title' => 'georgian'], 'en' => ['id' => 2, 'title' => 'english'], 'ru' => ['id' => 3, 'title' => 'russian']]));
define('ADMIN_LANGS_DIR', URL . 'langs/');
define('ADMIN_DEFAULT_LANG', 'ka');
define('SITE_URL', str_replace('admin/', '', URL));
define('CONTENT_PER_PAGE', 50);

define('SLIDER_WIDTH_L', 1600);
define('SLIDER_HEIGHT_L', 375);
define('SLIDER_WIDTH_S', 100);
define('SLIDER_HEIGHT_S', 100);

define('MENU_COVER_WIDTH_L', 1600);
define('MENU_COVER_HEIGHT_L', 375);